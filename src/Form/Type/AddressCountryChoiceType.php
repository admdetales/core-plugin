<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Form\Type;

use Sylius\Component\Addressing\Model\CountryInterface;
use Sylius\Component\Channel\Context\ChannelContextInterface;
use Sylius\Component\Resource\Repository\RepositoryInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

final class AddressCountryChoiceType extends AbstractType
{
    /**
     * @var RepositoryInterface
     */
    private $countryRepository;

    /**
     * @var ChannelContextInterface
     */
    private $channelContext;

    /**
     * AddressCountryChoiceType constructor.
     *
     * @param RepositoryInterface     $countryRepository
     * @param ChannelContextInterface $channelContext
     */
    public function __construct(
        RepositoryInterface $countryRepository,
        ChannelContextInterface $channelContext
    )
    {
        $this->countryRepository = $countryRepository;
        $this->channelContext= $channelContext;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'choice_filter' => null,
                'choices' => function (Options $options): iterable {
                    // Get countries from omni_channel_country.
                    $countries = $this->channelContext->getChannel()->getCountries()->toArray();

                    if (empty($countries)) {
                        // No countries assigned to channel - show enabled from Country table.
                        $countries = $this->countryRepository->findBy(['enabled' => true]);
                    }

                    usort($countries, function (CountryInterface $a, CountryInterface $b): int {
                        return $a->getName() <=> $b->getName();
                    });

                    return $countries;
                },
                'choice_value' => 'code',
                'choice_label' => 'name',
                'choice_translation_domain' => false,
                'enabled' => true,
                'label' => 'sylius.form.address.country',
                'placeholder' => 'sylius.form.country.select',
            ])
            ->setAllowedTypes('choice_filter', ['null', 'callable'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(): string
    {
        return ChoiceType::class;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'omni_address_country_choice';
    }
}
