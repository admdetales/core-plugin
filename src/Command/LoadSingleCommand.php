<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Omni\Sylius\CorePlugin\Command;

use Sylius\Bundle\FixturesBundle\Fixture\FixtureInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class LoadSingleCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('omni_sylius:fixtures:load_single')
            ->setDescription('Loads single fixture without dropping the database')
            ->addArgument('suite', InputArgument::REQUIRED)
            ->addArgument('fixture', InputArgument::REQUIRED)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $io = new SymfonyStyle($input, $output);

        $io->note('This command does not purge the database before running the fixture. Please use caution');
        $suite = $this->getContainer()->get('sylius_fixtures.suite_registry')->getSuite($input->getArgument('suite'));

        if (!$suite) {
            $io->error('No suite found: ' . $input->getArgument('suite'));
        }

        /** @var FixtureInterface $fixture */
        /** @var array $fixtureOptions */
        foreach ($suite->getFixtures() as $fixture => $fixtureOptions) {
            if ($fixture->getName() == $input->getArgument('fixture')) {
                $this->getContainer()->get('sylius_fixtures.fixture_loader')->load($suite, $fixture, $fixtureOptions);
                $io->success('Fixture loaded successfully');

                return;
            }
        }

        $io->warning('No fixture found: ' . $input->getArgument('fixture'));
    }
}
